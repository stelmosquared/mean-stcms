'use strict';

/**
 * @ngdoc function
 * @name fullstackApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the fullstackApp
 */
angular.module('fullstackApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
