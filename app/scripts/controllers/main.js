'use strict';

/**
 * @ngdoc function
 * @name fullstackApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fullstackApp
 */
angular.module('fullstackApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
